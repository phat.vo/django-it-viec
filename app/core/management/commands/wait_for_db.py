import time

from django.db import connections
from django.core.management.base import BaseCommand

class Command(BaseCommand):
    def handle(self,*args,**options):
        self.stdout.write(' WARNING: for database')
        db_conn=None
        while not db_conn:
            try:
                db_conn=connections['default']
            except:
                self.stdout.write(' database unavaible')
                time.sleep(1)
        self.stdout.write(self.style.SUCCESS('database available'))

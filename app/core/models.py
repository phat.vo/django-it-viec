from django.db import models

from django.contrib.auth.models import AbstractBaseUser,BaseUserManager,PermissionsMixin
from django.conf import settings

class UserManager(BaseUserManager):
    def create_user(self,email,password=None,**extra_fields):
        if not email:
            raise ValueError("user  must enter email")
        """create and save new user"""
        user=self.model(email=self.normalize_email(email),**extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user
    def create_superuser(self,email,password):
        """create suoer"""
        user=self.create_user(email,password)
        user.is_staff=True
        user.is_superuser=True
        user.save(using=self._db)
        return user
class User(AbstractBaseUser,PermissionsMixin):
    """Custom user model """

    email=models.EmailField(max_length=255,unique=True)
    name=models.CharField(max_length=255)
    is_active=models.BooleanField(default=True)
    is_staff=models.BooleanField(default=False)

    objects=UserManager()

    USERNAME_FIELD='email'
# Create your models here.
class Language(models.Model):

    name= models.CharField(max_length=255)
    user=models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return self.name
class Company(models.Model):
    name= models.CharField(max_length=255,default=None)
    user=models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
    )
    def __str__(self):
        return self.name
class Job(models.Model):
    options=(
    ('nonactive','Nonactive'),
    ('publish','Publish')
    )
    user=models.ForeignKey(settings.AUTH_USER_MODEL,
    on_delete=models.CASCADE)
    title=models.CharField(max_length=255)
    salary=models.DecimalField(max_digits=10,decimal_places=2)
    description=models.TextField()
    languages=models.ManyToManyField('Language')
    company=models.ForeignKey('Company',on_delete=models.CASCADE)
    status= models.CharField(
    max_length=10,choices=options,default='nonactive'
    )


    def __str__(self):
        return self.title

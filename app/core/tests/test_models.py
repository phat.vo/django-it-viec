from django.test import TestCase

from django.contrib.auth import get_user_model
from core import models

def sample_user(email='phatvo@gmail.com',password='testpass'):
    return get_user_model().objects.create_user(email,password)


class ModelTests(TestCase):
    def test_create_user_with_email_successful(self):
        """test create a new user with email"""
        email="phatbb@gmail.com"
        password='phat123'
        user=get_user_model().objects.create_user(
        email=email ,
        password=password
        )
        self.assertEqual(user.email,email)
        self.assertTrue(user.check_password(password))
    def test_new_user_invalid_email(self):
        with self.assertRaises(ValueError):
            get_user_model().objects.create_user(None, "test124")

    def test_create_new_superuser(self):
        user=get_user_model().objects.create_superuser(
        "phat@yahoo.com",
        "phat123"
        )
        self.assertTrue(user.is_superuser)
        self.assertTrue(user.is_staff)
    def test_language_tag_str(self):
        """test the tag representation"""
        language= models.Language.objects.create(
        user=sample_user(),
        name='python'

        )
        self.assertEqual(str(language),language.name)

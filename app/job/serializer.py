from rest_framework import serializers

from core.models import Language,Company,Job

class LanguageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Language
        fields=('id','name')
        read_only_fields=('id',)
class CompanySerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields=('id','name')
        read_only_fields=('id',)
class JobSerializer(serializers.ModelSerializer):
    languages=serializers.PrimaryKeyRelatedField(
    many=True,
    queryset=Language.objects.all()
    )
    company=serializers.PrimaryKeyRelatedField(
    queryset=Company.objects.all()
    )
    class Meta:
        model = Job
        fields=('id','title','salary','description','languages','company')
        read_only_fields =('id',)

class JobDetailSerializer(JobSerializer):
    languages =LanguageSerializer(many=True,read_only=True)
    company=CompanySerializer(read_only=True)

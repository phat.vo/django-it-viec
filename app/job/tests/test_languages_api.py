from django.contrib.auth import get_user_model
from django.urls import reverse

from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from core.models import Language

from job.serializer import LanguageSerializer

LANGUAGE_URL = reverse('job:language-list')


class PublicLanguagesApiTests(TestCase):

    def setUp(self):
        self.client = APIClient()

    def test_login_required(self):
        res =self.client.get(LANGUAGE_URL)
        self.assertEqual(res.status_code,status.HTTP_401_UNAUTHORIZED)
class PrivateLanguagesApiTests(TestCase):
    def setUp(self):
        self.user=get_user_model().objects.create_user(
        'test@gmail.com',
        'password123'
        )
        self.client=APIClient()
        self.client.force_authenticate(self.user)
    def test_retrieve_languages(self):
        Language.objects.create(user=self.user,name='java')
        Language.objects.create(user=self.user,name='python')
        res=self.client.get(LANGUAGE_URL)
        languages=Language.objects.all().order_by('-name')
        serializer = LanguageSerializer(languages,many=True)
        self.assertEqual(res.status_code,status.HTTP_200_OK)
        self.assertEqual(res.data,serializer.data)

    def test_languages_limited_to_user(self):
        """Test that tags returned are for authenticated user"""
        user2 = get_user_model().objects.create_user(
            'other@londonappdev.com',
            'testpass'
        )
        Language.objects.create(user=user2, name='Fruity')
        language = Language.objects.create(user=self.user, name='C++')

        res = self.client.get(LANGUAGE_URL)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 1)
        self.assertEqual(res.data[0]['name'], language.name)

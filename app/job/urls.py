from django.urls import path,include
from rest_framework.routers import DefaultRouter

from job import views

router = DefaultRouter()
router.register('language',views.LanguageViewSet)
router.register('company',views.CompanyViewSet)
router.register('job',views.JobViewSet)
app_name='job'

urlpatterns=[
path('',include(router.urls))
]

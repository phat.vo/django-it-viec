from rest_framework import viewsets,mixins
from rest_framework.response import Response

from rest_framework.authentication import TokenAuthentication
from rest_framework.permissions import IsAuthenticated,IsAdminUser,IsAuthenticatedOrReadOnly

from core.models import Language,Company,Job
# Create your views here.
from . import serializer
from rest_framework.decorators import action




class BaseJobAttrViewSet(viewsets.GenericViewSet,mixins.ListModelMixin,mixins.CreateModelMixin):
    """base viewset for job attr"""
    authentication_classes=(TokenAuthentication,)
    permission_classes=(IsAuthenticated,)
    def get_queryset(self):
        return self.queryset.filter(user=self.request.user).order_by('-name')
    def perform_create(self,serializer):
        serializer.save(user=self.request.user)


class LanguageViewSet(BaseJobAttrViewSet):

    queryset=Language.objects.all()
    serializer_class=serializer.LanguageSerializer

class CompanyViewSet(BaseJobAttrViewSet):

    queryset=Company.objects.all()
    serializer_class=serializer.CompanySerializer
class JobViewSet(viewsets.ModelViewSet):
    serializer_class=serializer.JobSerializer
    queryset=Job.objects.all()
    authentication_classes=(TokenAuthentication,)
    permission_classes=(IsAuthenticatedOrReadOnly,)
    def get_queryset(self):
        return self.queryset.filter(user=self.request.user,status='nonactive')
        #only user c
    def get_serializer_class(self):
        if self.action == 'retrieve':
            return serializers.JobDetailSerializer
        return self.serializer_class



    def perform_create(self,serializer):
        serializer.save(user=self.request.user)

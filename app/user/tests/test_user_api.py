from django.test import TestCase

from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework.test import APIClient
from rest_framework import status


CREATE_USER_URLS = reverse('user:create')
TOKEN_URL = reverse('user:token')

def create_user(**params):
     return get_user_model().objects.create_user(**params)

class PublicUserApiTest(TestCase):
    def setUp(self):
        self.client = APIClient()

    def test_create_valid_user_success(self):
        payload={
        'email':'test@gmail.com',
        'password':'tesdt123',
        'name':'phatvo'
        }
        res=self.client.post(CREATE_USER_URLS,payload)
        self.assertEqual(res.status_code,status.HTTP_201_CREATED)

    """def test_create_token_for_user(self):

        payload = {'email': 'test@londonappdev.com', 'password': 'testpass'}
        create_user(**payload)
        res = self.client.post(TOKEN_URL, payload)

        self.assertIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)"""

    def test_create_token_invalid_credentials(self):
        create_user(email='test@londonappdev.com', password='testpass')
        payload = {'email': 'test@londonappdev.com', 'password': 'wrong'}
        res = self.client.post(TOKEN_URL, payload)

        self.assertNotIn('token', res.data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
